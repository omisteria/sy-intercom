package com.synapse.ms.intercom;

import com.synapse.ms.intercom.services.ServiceManager;
import com.synapse.ms.intercom.services.tts.TtsClient;
import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;

@Replaces(ServiceManager.class)
@Requires(env = Environment.TEST)
public class MockServiceManager extends ServiceManager {

    @Override
    public void sayText(String sinkName, int volume_percent, String text) throws IOException {
        // empty
        try {
            getTtsClient().sendText(sinkName+"="+volume_percent+"="+text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
