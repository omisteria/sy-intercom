package com.synapse.ms.intercom;

import com.synapse.ms.intercom.services.ServiceManager;
import io.micronaut.context.ApplicationContext;
import io.micronaut.context.BeanContext;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.client.HttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.inject.Inject;

import java.io.IOException;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class ApiControllerTest {

    private static EmbeddedServer server;
    private static HttpClient client;

    @BeforeClass
    public static void setupServer() {
        server = ApplicationContext
                .run(EmbeddedServer.class);
        client = server
                .getApplicationContext()
                .createBean(HttpClient.class, server.getURL());
    }

    @AfterClass
    public static void stopServer() {
        if(server != null) {
            server.stop();
        }
        if(client != null) {
            client.stop();
        }
    }

    /*@Before
    public void prep() {
        try {
            serviceManager.sayText("r1111", 65000, "test");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Test
    public void ep_sayText() throws Exception {
        HttpResponse res = client.toBlocking().exchange("/api/saytext?scope=r1111&volume=65000&text=test");
        System.out.println(res);
        assertEquals(
                res.status().getCode(),
                HttpResponse.ok().code()
        );
    }

    @Test
    public void ep_log() throws Exception {
        String body = client.toBlocking().retrieve("/api/log");
        assertNotNull(body);
        assertEquals(
                body,
                "[]"
        );
    }
}
