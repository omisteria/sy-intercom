package com.synapse.ms.intercom;

import com.synapse.ms.intercom.services.tts.FestivalTtsClient;
import com.synapse.ms.intercom.services.tts.TtsClient;
import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;


@Replaces(FestivalTtsClient.class)
@Requires(env = Environment.TEST)
public class MockTtsClient extends FestivalTtsClient {

    static String stringLocal;

    @Override
    public void sendText(String text) throws Exception {
        stringLocal = text;
    }

    public String getInputText() {
        return stringLocal;
    }
}
