package com.synapse.ms.intercom;

import com.synapse.ms.intercom.services.ServiceManager;
import io.micronaut.http.HttpResponse;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;


import javax.inject.Inject;
import javax.inject.Named;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@MicronautTest
public class MessagesTest {

    @Inject
    private ServiceManager serviceManager;

    @Inject
    @Named("mqttAdapter")
    private MqttAdapter mqttAdapter;

    @Test
    public void mqtt_sayText() throws Exception {
        MockTtsClient ttsClient = (MockTtsClient) serviceManager.getTtsClient();
        //serviceManager.sayText("r1111", 65000, "test");

        Map<String, String> data = new HashMap<>();
        data.put("action", "saytext");
        data.put("text", "mqtt");

        mqttAdapter.publishMessage("service/text2voice", data);

        Thread.sleep(5000);

        String res = ttsClient.getInputText();
        assertEquals(
                res,
                "r1111=65000=mqtt"
        );
    }
}
