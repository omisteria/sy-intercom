package com.synapse.ms.intercom.models;

import java.util.Date;

public class InputRequest {

    private Date regDate;

    private String message;

    public InputRequest(String message) {
        regDate = new Date();
        this.message = message;
    }

    public Date getRegDate() {
        return regDate;
    }

    public String getMessage() {
        return message;
    }
}
