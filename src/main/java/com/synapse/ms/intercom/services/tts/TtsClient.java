package com.synapse.ms.intercom.services.tts;

public interface TtsClient {

    void sendText(String text) throws Exception;
}
