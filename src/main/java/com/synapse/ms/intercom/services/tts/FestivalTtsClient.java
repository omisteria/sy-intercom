package com.synapse.ms.intercom.services.tts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

@Singleton
public class FestivalTtsClient implements TtsClient {

    private static final Logger logger = LoggerFactory.getLogger(FestivalTtsClient.class);

    public void sendText(String text) throws Exception {

        logger.debug("bash command = " + text);

        try {

            Runtime.getRuntime().exec(new String[]{"/bin/bash", "-c", text});

        } catch (Exception e) {
            logger.debug("Error of runtime exec", e);
        }

    }

    public void sendText2(String text)
    {
        Socket socket = null;
        PrintWriter writer = null;

        text = fixString(text);

        try
        {
            socket = new Socket("192.168.3.10", 1314);
            writer = new PrintWriter(socket.getOutputStream(),true);

            logger.info("Print text: '"+text+"'");

            //sendLine("(Parameter.set 'Wavefiletype '" + format + ")\n");	//Tells Festival what format to return
            //sendLine("(Parameter.set 'Language 'russian)");
            //sendLine("(language_russian)\n");
            //sendLine("(voice_msu_ru_nsh_clunits)");		//Optional: selects a voice. Otherwise, the default voice is used
            //sendLine("(tts_return_to_client)\n");
            //sendLine("(tts_textall \"\n" + text + "\" \"fundamental\")\n");
            //sendLine("(begin (language_russian) (voice_msu_ru_nsh_clunits) (SayText \""+text+"\"))");

            writer.println("(SayText \""+text+"\")");
            writer.flush();

        } catch (Exception e) {

            logger.error(e.getMessage(), e);

        } finally {
            try {
                writer.close();
                socket.close();
            } catch (Exception e) {
                // empty
            }
        }
    }


    private String fixString(String text)
    {
        text = text.trim();

        int loc1 = text.indexOf('"');
        int loc2 = text.indexOf('\\');
        if (loc1 == -1 && loc2 == -1)
            return text;

        StringBuffer s = new StringBuffer();
        for (int i=0; i<text.length(); i++)
        {
            if (text.charAt(i) == '"' || text.charAt(i) == '\\')
                s.append('\\');
            s.append(text.charAt(i));
        }

        return s.toString();
    }

    /*public static void main(String[] args) {
        new FestivalClient().sendTextToServer("Test");
    }*/
}
