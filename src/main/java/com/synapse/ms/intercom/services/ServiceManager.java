package com.synapse.ms.intercom.services;

import com.synapse.ms.intercom.models.InputRequest;
import com.synapse.ms.intercom.services.pulseaudio.PulseAudio;
import com.synapse.ms.intercom.services.tts.FestivalTtsClient;
import com.synapse.ms.intercom.services.tts.TtsClient;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Singleton
public class ServiceManager {

    private static final Logger logger = LoggerFactory.getLogger(ServiceManager.class);

    private CircularFifoQueue<InputRequest> recentRequests = new CircularFifoQueue<>(40);

    ExecutorService executorService = Executors.newFixedThreadPool(1);

    @Inject
    private FestivalTtsClient ttsClient;

    public void setVolume(String scopeId, int volume) throws IOException {
        logger.info("Set scope "+scopeId);
        PulseAudio.setDefaultSink(scopeId);

        logger.info("Set volume " + volume);
        PulseAudio.setVolume(scopeId, volume);
    }

    public List<InputRequest> getRecentRequests() {
        List log = new ArrayList<>(recentRequests);
        Collections.reverse(log);
        return log;
    }

    public void sayText(String sinkName, int volume_percent, String text) throws Exception {

        logger.debug("Text for service: " + text);

        // log of request
        recentRequests.add(new InputRequest(text));

        PulseAudio.getListSink(logger);

        int volume = 65535 / 100 * volume_percent;
        String bashCommand = "/opt/saytext " + sinkName + " " + volume + " \"" + text + "\"";

        ttsClient.sendText(bashCommand);
    }

    public void sayText_not_woking(String sinkName, int volume_percent, String text) throws IOException {

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                PulseAudio.getListSink(logger);

                int volume = 65535 / 100 * volume_percent;
                String bashCommand = "/opt/saytext " + sinkName + " " + volume + " \"" + text + "\"";

                logger.info("bash command = " + bashCommand);

                Process p = null;
                try {
                    p = Runtime.getRuntime().exec(new String[]{"/bin/bash", "-c", bashCommand});
                    p.waitFor();

                    Thread.sleep(1000);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void runSoundServer() throws IOException {

        String bashCommand = "/usr/bin/pulseaudio --start -D";

        Runtime.getRuntime().exec(new String[]{"/bin/bash", "-c", bashCommand});
    }

    public TtsClient getTtsClient() {
        return ttsClient;
    }
}
