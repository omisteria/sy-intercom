package com.synapse.ms.intercom.controllers;

import com.synapse.ms.intercom.models.InputRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;

import java.util.List;

@Client("/api")
public interface ApiClient {

    @Get(value = "/saytext")
    HttpResponse sayText(String scope, Integer volume, String text);

    @Get(value = "/log", produces = MediaType.APPLICATION_JSON)
    List<InputRequest> getRecentRequests();
}
