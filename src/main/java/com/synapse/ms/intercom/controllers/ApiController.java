package com.synapse.ms.intercom.controllers;

import com.synapse.ms.intercom.models.InputRequest;
import com.synapse.ms.intercom.services.ServiceManager;
import io.micronaut.context.annotation.Parameter;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

@Controller("/api")
public class ApiController {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Inject
    private ServiceManager serviceManager;

    @Get(value = "/saytext")
    public HttpResponse sayText(@Parameter String scope,
                          @Parameter Integer volume,
                          @Parameter String text) {

        logger.debug("say text : {}", text);

        try {
            serviceManager.sayText(scope, volume, text);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);

            return HttpResponse.badRequest(e.getMessage());
        }

        return HttpResponse.ok();
    }

    @Get(value = "/log", produces = MediaType.APPLICATION_JSON)
    public List<InputRequest> getRecentRequests() {
        return  serviceManager.getRecentRequests();
    }
}

