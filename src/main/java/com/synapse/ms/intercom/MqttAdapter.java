package com.synapse.ms.intercom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synapse.ms.intercom.services.ServiceManager;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Value;
import io.micronaut.discovery.event.ServiceShutdownEvent;
import io.micronaut.discovery.event.ServiceStartedEvent;
import io.micronaut.runtime.event.annotation.EventListener;
import io.micronaut.scheduling.annotation.Async;
import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Context
public class MqttAdapter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${spring.server.mqtt.url}")
    private String mqttUrl;

    @Value("${spring.server.mqtt.client}")
    private String clientName;

    @Value("${spring.server.mqtt.topic}")
    private String mainTopic;

    @Value("${spring.server.mqtt.qos:2}")
    private Integer mqttQos;

    @Inject
    private ServiceManager serviceManager;

    private IMqttClient mqttClient;

    public MqttAdapter() {

    }

    public IMqttClient createMqttClient() throws MqttException {
        IMqttClient mqttClient = new MqttClient(mqttUrl, clientName + "-" + UUID.randomUUID().toString());
        return mqttClient;
    }

    public MqttConnectOptions connectionOptions() throws MqttException {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        return options;
    }

    public void publishMessage(String topic, String content) throws MqttException {
        MqttMessage message = new MqttMessage(content.getBytes());
        message.setQos(mqttQos);
        mqttClient.publish(topic, message);
    }

    public void publishMessage(String topic, Map content) throws MqttException {
        try {
            String payload = new ObjectMapper().writeValueAsString(content);

            MqttMessage message = new MqttMessage(payload.getBytes());
            message.setQos(mqttQos);
            mqttClient.publish(topic, message);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    @EventListener
    @Async
    public void connectMqttServer(final ServiceStartedEvent event) {

        logger.info("MQTT Connecting ...");

        try {
            this.mqttClient = createMqttClient();
            this.mqttClient.connect(connectionOptions());

            this.mqttClient.subscribe(mainTopic, (topic, message) -> {

                logger.info("Got message: " + message.getPayload());

                try {
                    String utf8string = new String(message.getPayload(), "UTF-8");

                    Map<String, String> messageMap = new ObjectMapper().readValue(utf8string, HashMap.class);
                    String action = messageMap.get("action");
                    String text = messageMap.get("text");

                    if (action != null && "saytext".equalsIgnoreCase(action) && text != null) {
                        serviceManager.sayText("r1111", 100, text);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @EventListener
    public void disconnectMqttServer(final ServiceShutdownEvent event) {

        logger.info("MQTT Disconnecting ...");

        try {
            mqttClient.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
