FROM openjdk:8u171-alpine3.7
RUN apk --no-cache add curl
COPY target/intercom*.jar sy-intercom.jar
CMD java ${JAVA_OPTS} -jar sy-intercom.jar